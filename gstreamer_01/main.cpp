#include <gst/gst.h>

struct PlayPipeline {
    GstElement *audioQueue;
    GstElement *audioDecode;
    GstElement *audioConvert;
    GstElement *audioResample;
    GstElement *audioSink;
    GstElement *videoQueue;
    GstElement *videoDecode;
    GstElement *videoConvert;
    GstElement *videoscale;
    GstElement *videoSink;
};
struct TranscodePipeline {
    GstElement *audioQueue;
    GstElement *audioDecode;
    GstElement *audioConvert;
    GstElement *audioResample;
    GstElement *audioEncoder;
    GstElement *videoQueue;
    GstElement *videoParse;
    GstElement *matroskamux;
    GstElement *output;
};
struct CustomData {
    GstElement *pipeline;
    GstElement *source;
    GstElement *qtdemux;
    GstElement *audioQueue;
    GstElement *videoQueue;
    GstElement *audioTee;
    GstElement *videoTee;
    PlayPipeline playMedia;
    TranscodePipeline transcodeMedia;
};

static void pad_added_handler(GstElement *src, GstPad *pad, CustomData *data);
int create_elements(CustomData& data, char *argv[]);
int link_elements_many(CustomData& data);

int main(int argc, char *argv[]) {
    CustomData data;
    GstBus *bus;
    GstMessage *msg;
    int ret = 0;

    gst_init(&argc, &argv);

    ret = create_elements(data, argv);
    if(ret != 0) {
        return ret;
    }
    
    ret = link_elements_many(data);
    if(ret != 0) {
        return ret;
    }

    GstPad *audioPlayQueuePad, *audioTranscodeQueuePad, *videoPlayQueuePad, *videoTranscodeQueuePad;
    GstPad *audioPlayTeePad, *audioTranscodeTeePad, *videoPlayTeePad, *videoTranscodeTeePad;
    audioPlayQueuePad = gst_element_get_static_pad(data.playMedia.audioQueue, "sink");
    audioTranscodeQueuePad = gst_element_get_static_pad(data.transcodeMedia.audioQueue, "sink");
    videoPlayQueuePad = gst_element_get_static_pad(data.playMedia.videoQueue, "sink");
    videoTranscodeQueuePad = gst_element_get_static_pad(data.transcodeMedia.videoQueue, "sink");
    audioPlayTeePad = gst_element_request_pad_simple(data.audioTee, "src_%u");
    audioTranscodeTeePad = gst_element_request_pad_simple(data.audioTee, "src_%u");
    videoPlayTeePad = gst_element_request_pad_simple(data.videoTee, "src_%u");
    videoTranscodeTeePad = gst_element_request_pad_simple(data.videoTee, "src_%u");
    if (gst_pad_link(audioPlayTeePad, audioPlayQueuePad) != GST_PAD_LINK_OK ||
        gst_pad_link(audioTranscodeTeePad, audioTranscodeQueuePad) != GST_PAD_LINK_OK ||
        gst_pad_link(videoPlayTeePad, videoPlayQueuePad) != GST_PAD_LINK_OK ||
        gst_pad_link(videoTranscodeTeePad, videoTranscodeQueuePad) != GST_PAD_LINK_OK)
    {
        g_printerr("Tee could not be linked.\n");
        gst_object_unref(data.pipeline);
        return -1;
    }
    gst_object_unref (audioPlayQueuePad);
    gst_object_unref (audioTranscodeQueuePad);
    gst_object_unref (videoPlayQueuePad);
    gst_object_unref (videoTranscodeQueuePad);

    GstPad *audioEncoderPad, *videoParserPad; 
    GstPad *audioMuxerPad, *videoMuxerPad;
    audioEncoderPad = gst_element_get_static_pad(data.transcodeMedia.audioEncoder, "src");
    videoParserPad = gst_element_get_static_pad(data.transcodeMedia.videoParse, "src");
    audioMuxerPad = gst_element_request_pad_simple(data.transcodeMedia.matroskamux, "audio_%u");
    videoMuxerPad = gst_element_request_pad_simple(data.transcodeMedia.matroskamux, "video_%u");
    if (gst_pad_link(audioEncoderPad, audioMuxerPad) != GST_PAD_LINK_OK ||
        gst_pad_link(videoParserPad, videoMuxerPad) != GST_PAD_LINK_OK)
    {
        g_printerr("Matroskamux could not be linked.\n");
        gst_object_unref(data.pipeline);
        return -1;
    }
    gst_object_unref (audioEncoderPad);
    gst_object_unref (videoParserPad);

    g_signal_connect(data.qtdemux, "pad-added", G_CALLBACK(pad_added_handler), &data);

    gst_element_set_state(data.pipeline, GST_STATE_PLAYING);
    
    bus = gst_element_get_bus(data.pipeline);
    msg = gst_bus_timed_pop_filtered(bus, GST_CLOCK_TIME_NONE, (GstMessageType)(GST_MESSAGE_ERROR | GST_MESSAGE_EOS));

    gst_element_release_request_pad(data.audioTee, audioPlayTeePad);
    gst_element_release_request_pad(data.audioTee, audioTranscodeTeePad);
    gst_element_release_request_pad(data.videoTee, videoPlayTeePad);
    gst_element_release_request_pad(data.videoTee, videoTranscodeTeePad);
    gst_object_unref(audioPlayTeePad);
    gst_object_unref(audioTranscodeTeePad);
    gst_object_unref(videoPlayTeePad);
    gst_object_unref(videoTranscodeTeePad);
    gst_element_release_request_pad(data.transcodeMedia.matroskamux, audioMuxerPad);
    gst_element_release_request_pad(data.transcodeMedia.matroskamux, videoMuxerPad);
    gst_object_unref(audioMuxerPad);
    gst_object_unref(videoMuxerPad);

    if (msg != NULL)
        gst_message_unref(msg);

    gst_object_unref(bus);
    gst_element_set_state(data.pipeline, GST_STATE_NULL);

    gst_object_unref(data.pipeline);
    return 0;
}

static void pad_added_handler(GstElement *src, GstPad *newPad, CustomData *data) {
    GstPad *audioSinkPad = gst_element_get_static_pad(data->audioQueue, "sink");
    GstPad *videoSinkPad = gst_element_get_static_pad(data->videoQueue, "sink");
    GstPadLinkReturn ret;
    GstCaps *newPadCaps = NULL;
    GstStructure *newPadStruct = NULL;
    const gchar *newPadType = NULL;
    const gchar *newPadName = GST_PAD_NAME(newPad);

    newPadCaps = gst_pad_get_current_caps(newPad);
    newPadStruct = gst_caps_get_structure(newPadCaps, 0);
    newPadType = gst_structure_get_name(newPadStruct);

    if (g_str_has_prefix(newPadName, "video_0")) {
        if (!gst_pad_is_linked(videoSinkPad) && g_str_has_prefix(newPadType, "video/x-h264")) {
            ret = gst_pad_link(newPad, videoSinkPad);
            if (GST_PAD_LINK_FAILED(ret)) {
                g_print("Type is '%s' but link failed.\n", newPadType);
            }
        }
    }
    if (g_str_has_prefix(newPadName, "audio_0")) {
        if (!gst_pad_is_linked(audioSinkPad) && g_str_has_prefix(newPadType, "audio/mpeg")) {
            ret = gst_pad_link(newPad, audioSinkPad);
            if (GST_PAD_LINK_FAILED(ret)) {
                g_print("Type is '%s' but link failed.\n", newPadType);
            }
        }
    }

    if (newPadCaps != NULL)
        gst_caps_unref(newPadCaps);

    gst_object_unref(audioSinkPad);
    gst_object_unref(videoSinkPad);
}

int create_elements(CustomData& data, char *argv[]) {
    data.source = gst_element_factory_make("filesrc", "source");
    data.qtdemux = gst_element_factory_make("qtdemux", "demux");
    data.audioQueue = gst_element_factory_make("queue", "audioQueue");
    data.videoQueue = gst_element_factory_make("queue", "videoQueue");
    data.audioTee = gst_element_factory_make("tee", "audioTee");
    data.videoTee = gst_element_factory_make("tee", "videoTee");
    
    data.playMedia.audioQueue = gst_element_factory_make("queue", "playAudioQueue");
    data.playMedia.audioDecode = gst_element_factory_make("faad", "playAudioDecode");
    data.playMedia.audioConvert = gst_element_factory_make("audioconvert", "playAudioConvert");
    data.playMedia.audioResample = gst_element_factory_make("audioresample", "playAudioResample");
    data.playMedia.audioSink = gst_element_factory_make("autoaudiosink", "audioSink");
    data.playMedia.videoQueue = gst_element_factory_make("queue", "playVideoQueue");
    data.playMedia.videoDecode = gst_element_factory_make("avdec_h264", "videoDecode");
    data.playMedia.videoConvert = gst_element_factory_make("videoconvert", "videoConvert");
    data.playMedia.videoscale = gst_element_factory_make("videoscale", "videoscale");
    data.playMedia.videoSink = gst_element_factory_make("autovideosink", "videoSink");
    
    data.transcodeMedia.audioQueue = gst_element_factory_make("queue", "transcodeAudioQueue");
    data.transcodeMedia.audioDecode = gst_element_factory_make("faad", "transcodeAudioDecode");
    data.transcodeMedia.audioConvert = gst_element_factory_make("audioconvert", "transcodeAudioConvert");
    data.transcodeMedia.audioResample = gst_element_factory_make("audioresample", "transcodeAudioResample");
    data.transcodeMedia.audioEncoder = gst_element_factory_make("vorbisenc", "audioEncoder");
    data.transcodeMedia.videoQueue = gst_element_factory_make("queue", "transcodeVideoQueue");
    data.transcodeMedia.videoParse = gst_element_factory_make("h264parse", "videoParser");
    data.transcodeMedia.matroskamux = gst_element_factory_make("matroskamux", "matroskaMux");
    data.transcodeMedia.output = gst_element_factory_make("filesink", "output");

    data.pipeline = gst_pipeline_new("test-pipeline");

    if (!data.pipeline || !data.source || !data.qtdemux || !data.audioQueue || !data.videoQueue || !data.videoTee || !data.audioTee ||
        !data.playMedia.audioQueue || !data.playMedia.audioConvert || !data.playMedia.audioResample || !data.playMedia.audioSink ||
        !data.playMedia.videoQueue || !data.playMedia.videoConvert || !data.playMedia.videoscale || !data.playMedia.videoSink ||
        !data.transcodeMedia.audioQueue || !data.transcodeMedia.audioDecode || !data.transcodeMedia.audioConvert || 
        !data.transcodeMedia.audioResample || !data.transcodeMedia.audioEncoder ||
        !data.transcodeMedia.videoQueue || !data.transcodeMedia.videoParse || !data.transcodeMedia.matroskamux || !data.transcodeMedia.output) {
        g_printerr("Not all elements could be created.\n");
        return -1;
    }

    g_object_set(G_OBJECT(data.source), "location", argv[1], NULL);
    g_object_set(G_OBJECT(data.transcodeMedia.output), "location", argv[2], NULL);

    return 0;
}

int link_elements_many(CustomData& data) {
    gst_bin_add_many(GST_BIN(data.pipeline), data.source, data.qtdemux, data.audioQueue, data.videoQueue, data.audioTee, data.videoTee, 
                     data.playMedia.audioQueue, data.playMedia.audioDecode, data.playMedia.audioConvert, data.playMedia.audioResample, data.playMedia.audioSink,
                     data.playMedia.videoQueue, data.playMedia.videoDecode, data.playMedia.videoConvert, data.playMedia.videoscale, data.playMedia.videoSink,
                     data.transcodeMedia.audioQueue, data.transcodeMedia.audioDecode, data.transcodeMedia.audioConvert,
                     data.transcodeMedia.audioResample, data.transcodeMedia.audioEncoder, 
                     data.transcodeMedia.videoQueue, data.transcodeMedia.videoParse, data.transcodeMedia.matroskamux, data.transcodeMedia.output, NULL);
    if (gst_element_link_many(data.source, data.qtdemux, NULL) != TRUE ||
        gst_element_link_many(data.audioQueue, data.audioTee, NULL) != TRUE ||
        gst_element_link_many(data.videoQueue, data.videoTee, NULL) != TRUE ||
        gst_element_link_many(data.playMedia.audioQueue, data.playMedia.audioDecode, data.playMedia.audioConvert, data.playMedia.audioResample, data.playMedia.audioSink, NULL) != TRUE ||
        gst_element_link_many(data.playMedia.videoQueue, data.playMedia.videoDecode, data.playMedia.videoConvert, data.playMedia.videoscale, data.playMedia.videoSink, NULL) != TRUE ||
        gst_element_link_many(data.transcodeMedia.audioQueue, data.transcodeMedia.audioDecode, data.transcodeMedia.audioConvert, data.transcodeMedia.audioResample, data.transcodeMedia.audioEncoder, NULL) != TRUE ||
        gst_element_link_many(data.transcodeMedia.videoQueue, data.transcodeMedia.videoParse, NULL) != TRUE ||
        gst_element_link_many(data.transcodeMedia.matroskamux, data.transcodeMedia.output, NULL) != TRUE)
    {
        g_printerr("Elements could not be linked.\n");
        gst_object_unref(data.pipeline);
        return -1;
    }

    return 0;
}

# About the project
Console application. It opens an ‘mp4’ video file, displays it on the screen, plays back audio if available (video codec: H.264, audio codec: MPEG-4 AAC), and records it into an ‘mkv’ file (video codec: H.264, audio codec: Vorbis).
# Required
- Gstreamer
- CMake
# Usage
./gstreamer_01 /tmp/test.mp4 /tmp/output.mkv
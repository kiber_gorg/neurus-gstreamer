#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/video.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/imgproc.hpp>

#include "gstobjdetect.hpp"

using namespace cv;
using namespace std;
using namespace dnn;

#define DEFAULT_MODEL_WEIGHTS "/home/yolov3.weights"
#define DEFAULT_MODEL_CONFIG "/home/yolov3.cfg"
#define DEFAULT_NMS_THRESHOLD 0.45
#define DEFAULT_CONFIDENCE_THRESHOLD 0.45
#define BLOB_WIDTH 416
#define BLOB_HEIGHT 416

GST_DEBUG_CATEGORY_STATIC(gst_obj_detect_debug_category);
#define GST_CAT_DEFAULT gst_obj_detect_debug_category

G_DEFINE_TYPE_WITH_CODE(GstObjDetect, gst_obj_detect, GST_TYPE_VIDEO_FILTER,
                        GST_DEBUG_CATEGORY_INIT(
                            gst_obj_detect_debug_category,
                            "obj_detect",
                            0,
                            "debug category for obj_detect element"));
GST_ELEMENT_REGISTER_DEFINE(obj_detect, 
                            "obj_detect", 
                            GST_RANK_NONE, 
                            GST_TYPE_OBJ_DETECT);

static void gst_obj_detect_set_property(GObject *object,
                                        guint property_id,
                                        const GValue *value,
                                        GParamSpec *pspec);
static void gst_obj_detect_get_property(GObject *object,
                                        guint property_id,
                                        GValue *value,
                                        GParamSpec *pspec);
static void gst_obj_detect_dispose(GObject *object);
static void gst_obj_detect_finalize(GObject *object);

static gboolean gst_obj_detect_start(GstBaseTransform *trans);
static gboolean gst_obj_detect_stop(GstBaseTransform *trans);
static gboolean gst_obj_detect_set_info(GstVideoFilter *filter,
                                        GstCaps *incaps,
                                        GstVideoInfo *in_info,
                                        GstCaps *outcaps,
                                        GstVideoInfo *out_info);
static GstFlowReturn gst_obj_detect_transform_frame(GstVideoFilter *filter,
                                                    GstVideoFrame *inframe,
                                                    GstVideoFrame *outframe);
static GstFlowReturn gst_obj_detect_transform_frame_ip(GstVideoFilter *filter,
                                                       GstVideoFrame *frame);

enum
{
    PROP_0,
    PROP_DISPLAY,
    PROP_MODEL_WEIGHTS,
    PROP_MODEL_CONFIG,
    PROP_NMS_THRESHOLD,
    PROP_CONFIDENCE_THRESHOLD
};

static GstStaticPadTemplate sink_factory = GST_STATIC_PAD_TEMPLATE(
    "sink",
    GST_PAD_SINK,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS(GST_VIDEO_CAPS_MAKE ("BGR")));

static GstStaticPadTemplate src_factory = GST_STATIC_PAD_TEMPLATE(
    "src",
    GST_PAD_SRC,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS(GST_VIDEO_CAPS_MAKE ("BGR")));

static void gst_obj_detect_class_init(GstObjDetectClass *klass)
{
    GObjectClass *gobject_class; 
    GstBaseTransformClass *base_transform_class;
    GstVideoFilterClass *gstvideofilter_class;

    gobject_class = (GObjectClass *)klass;
    base_transform_class = (GstBaseTransformClass *)klass;
    gstvideofilter_class = (GstVideoFilterClass *)klass;

    gobject_class->set_property = gst_obj_detect_set_property;
    gobject_class->get_property = gst_obj_detect_get_property;
    gobject_class->dispose = gst_obj_detect_dispose;
    gobject_class->finalize = gst_obj_detect_finalize;
    base_transform_class->start = GST_DEBUG_FUNCPTR(gst_obj_detect_start);
    base_transform_class->stop = GST_DEBUG_FUNCPTR(gst_obj_detect_stop);
    gstvideofilter_class->set_info = GST_DEBUG_FUNCPTR(gst_obj_detect_set_info);
    gstvideofilter_class->transform_frame_ip = GST_DEBUG_FUNCPTR(gst_obj_detect_transform_frame_ip);

    g_object_class_install_property(gobject_class,
                                    PROP_DISPLAY,
                                    g_param_spec_boolean(
                                        "display",
                                        "Display",
                                        "Sets whether the detected object should be highlighted in the output",
                                        TRUE, (GParamFlags)(G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS)));

    g_object_class_install_property(gobject_class, PROP_MODEL_WEIGHTS,
                                    g_param_spec_string(
                                        "model-weights",
                                        "Yolov3 weight",
                                        "Path to the yolov3 .weight file",
                                        DEFAULT_MODEL_WEIGHTS,
                                        (GParamFlags)(G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS)));
    
    g_object_class_install_property(gobject_class, PROP_MODEL_CONFIG,
                                    g_param_spec_string(
                                        "model-config",
                                        "Yolov3 config",
                                        "Path to the yolov3 .config file",
                                        DEFAULT_MODEL_CONFIG,
                                        (GParamFlags)(G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS)));

    
    g_object_class_install_property(gobject_class, PROP_NMS_THRESHOLD,
                                    g_param_spec_float (
                                        "nms-treshold",
                                        "NMS treshold",
                                        "A threshold used in non maximum suppression",
                                        0.1, 1.0, DEFAULT_NMS_THRESHOLD,
                                        (GParamFlags)(G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS)));

    g_object_class_install_property(gobject_class, PROP_CONFIDENCE_THRESHOLD,
                                    g_param_spec_float (
                                        "confidence-treshold",
                                        "Confidence treshold",
                                        "A threshold used to filter low probability detections",
                                        0.1, 1.0, DEFAULT_CONFIDENCE_THRESHOLD,
                                        (GParamFlags)(G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS))); 

    gst_element_class_set_static_metadata(GST_ELEMENT_CLASS(klass),
                                          "Object detection example plugin",
                                          "Filter/Effect/Video",
                                          "Object Detection",
                                          "aboba <aboba@aboba>");

    gst_element_class_add_pad_template(GST_ELEMENT_CLASS(klass),
                                       gst_static_pad_template_get(&src_factory));
    gst_element_class_add_pad_template(GST_ELEMENT_CLASS(klass),
                                       gst_static_pad_template_get(&sink_factory));
}

void network_init(GstObjDetect *obj_detect)
{
    obj_detect->network = readNet(obj_detect->model_weights, obj_detect->model_config, "Darknet");
    obj_detect->network.setPreferableBackend(DNN_BACKEND_DEFAULT);
    obj_detect->network.setPreferableTarget(DNN_TARGET_CPU);
}

static void gst_obj_detect_init(GstObjDetect *obj_detect)
{
    obj_detect->display = TRUE;
    obj_detect->model_weights = g_strdup(DEFAULT_MODEL_WEIGHTS);
    obj_detect->model_config = g_strdup(DEFAULT_MODEL_CONFIG);
    obj_detect->nms_treshold = DEFAULT_NMS_THRESHOLD;
    obj_detect->confidence_treshold = DEFAULT_CONFIDENCE_THRESHOLD;
}

static void gst_obj_detect_set_property(GObject *object,
                                        guint property_id,
                                        const GValue *value,
                                        GParamSpec *pspec)
{
    GstObjDetect *detector = GST_OBJ_DETECT(object);

    GST_DEBUG_OBJECT(detector, "set_property");

    switch (property_id)
    {
        case PROP_DISPLAY:
            detector->display = g_value_get_boolean(value);
            break;
        case PROP_MODEL_WEIGHTS:
        {
            detector->model_weights = g_value_dup_string(value);
            break;
        }
        case PROP_MODEL_CONFIG:
        {
            detector->model_config = g_value_dup_string(value);
            break;
        }
        case PROP_NMS_THRESHOLD:
            detector->nms_treshold = g_value_get_float(value);
            break;
        case PROP_CONFIDENCE_THRESHOLD:
            detector->confidence_treshold = g_value_get_float(value);
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID(object, property_id, pspec);
            break;
    }
}

static void gst_obj_detect_get_property(GObject *object, 
                                 guint property_id,
                                 GValue *value, 
                                 GParamSpec *pspec)
{
    GstObjDetect *detector = GST_OBJ_DETECT(object);

    GST_DEBUG_OBJECT(detector, "get_property");

    switch (property_id)
    {
        case PROP_DISPLAY:
            g_value_set_boolean(value, detector->display);
            break;
        case PROP_MODEL_WEIGHTS:
            g_value_set_string(value, detector->model_weights);
            break;
        case PROP_MODEL_CONFIG:
            g_value_set_string(value, detector->model_config);
            break;
        case PROP_NMS_THRESHOLD:
            g_value_set_float(value, detector->nms_treshold);
            break;
        case PROP_CONFIDENCE_THRESHOLD:
            g_value_set_float(value, detector->confidence_treshold);
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID(object, property_id, pspec);
            break;
    }
}

static void gst_obj_detect_dispose(GObject *object)
{
    GstObjDetect *detector = GST_OBJ_DETECT(object);

    GST_DEBUG_OBJECT(detector, "dispose");

    /* clean up as possible.  may be called multiple times */

    G_OBJECT_CLASS(gst_obj_detect_parent_class)->dispose(object);
}

static void gst_obj_detect_finalize(GObject *object)
{
    GstObjDetect *detector = GST_OBJ_DETECT(object);

    GST_DEBUG_OBJECT(detector, "finalize");

    /* clean up object here */

    G_OBJECT_CLASS(gst_obj_detect_parent_class)->finalize(object);
}

static gboolean gst_obj_detect_start(GstBaseTransform *trans)
{
    GstObjDetect *detector = GST_OBJ_DETECT(trans);

    network_init(detector);

    GST_DEBUG_OBJECT(detector, "start");

    return TRUE;
}

static gboolean gst_obj_detect_stop(GstBaseTransform *trans)
{
    GstObjDetect *detector = GST_OBJ_DETECT(trans);

    GST_DEBUG_OBJECT(detector, "stop");

    return TRUE;
}

static gboolean gst_obj_detect_set_info(GstVideoFilter *filter,
                                        GstCaps *incaps,
                                        GstVideoInfo *in_info,
                                        GstCaps *outcaps,
                                        GstVideoInfo *out_info)
{
    GstObjDetect *detector = GST_OBJ_DETECT(filter);

    GST_DEBUG_OBJECT(detector, "set_info");

    return TRUE;
}

static GstFlowReturn gst_obj_detect_transform_frame(GstVideoFilter *filter,
                                                    GstVideoFrame *inframe,
                                                    GstVideoFrame *outframe)
{
    GstObjDetect *detector = GST_OBJ_DETECT(filter);

    GST_DEBUG_OBJECT(detector, "transform_frame");

    return GST_FLOW_OK;
}

void preprocess(const Mat& frame, Net& net, Size inpSize)
{
    Mat blob;
    blobFromImage(frame, blob, 1.0/255.0, inpSize, Scalar(), true, false);

    net.setInput(blob);
}

void postprocess(Mat &frame,
                 const std::vector<Mat> &outs,
                 const GstObjDetect &detector,
                 GstBuffer *buf)
{
    std::vector<int> classIds;
    std::vector<float> confidences;
    std::vector<Rect> boxes;

    for (size_t i = 0; i < outs.size(); ++i)
    {
        float *data = (float *)outs[i].data;
        for (int j = 0; j < outs[i].rows; ++j, data += outs[i].cols)
        {
            Mat scores = outs[i].row(j).colRange(5, outs[i].cols);
            Point classIdPoint;
            double confidence;
            minMaxLoc(scores, 0, &confidence, 0, &classIdPoint);
            if (confidence > detector.confidence_treshold)
            {
                int cx = (int)(data[0] * frame.cols);
                int cy = (int)(data[1] * frame.rows);
                int width = (int)(data[2] * frame.cols);
                int height = (int)(data[3] * frame.rows);
                int left = int(cx - 0.5 * width);
                int top = int(cy - 0.5 * height);

                classIds.push_back(classIdPoint.x);
                confidences.push_back((float)confidence);
                boxes.push_back(Rect(left, top, width, height));
            }
        }
    }
    std::vector<int> nmsIndices;
    NMSBoxes(boxes, confidences, detector.confidence_treshold,
             detector.nms_treshold, nmsIndices);
    std::vector<Rect> finalBoxes;
    std::vector<int> finalClassIds;
    for (size_t i = 0; i < nmsIndices.size(); i++)
    {
        size_t idx = nmsIndices[i];
        finalBoxes.push_back(boxes[idx]);
        finalClassIds.push_back(classIds[idx]);
    }

    if (detector.display)
    {
        for (size_t i = 0; i < finalBoxes.size(); ++i)
        {
            Rect box = finalBoxes[i];
            rectangle(frame,
                      Point(box.x, box.y),
                      Point(box.x + box.width, box.y + box.height),
                      Scalar(0, 255, 0));
            gst_buffer_add_video_region_of_interest_meta_id(buf,
                                                            (guint32)finalClassIds[i],
                                                            (guint)box.x,
                                                            (guint)box.y,
                                                            (guint)box.width,
                                                            (guint)box.height);
        }
    }
}

static GstFlowReturn gst_obj_detect_transform_frame_ip(GstVideoFilter *filter, GstVideoFrame *frame)
{
    GstObjDetect *detector = GST_OBJ_DETECT(filter);
    
    gint width = frame->info.width;
    gint height = frame->info.height;
    Mat img(Size(width, height), CV_8UC3, GST_VIDEO_FRAME_PLANE_DATA(frame, 0));

    preprocess(img, detector->network, Size(BLOB_WIDTH, BLOB_HEIGHT));

    std::vector<Mat> outs;
    detector->network.forward(outs, detector->network.getUnconnectedOutLayersNames());

    postprocess(img, outs, *detector, frame->buffer);

    GST_DEBUG_OBJECT(detector, "transform_frame_ip");

    return GST_FLOW_OK;
}

static gboolean plugin_init(GstPlugin *plugin)
{
    return gst_element_register(plugin, "obj_detect", GST_RANK_NONE, GST_TYPE_OBJ_DETECT);
}

#ifndef VERSION
#define VERSION "0.0.0"
#endif
#ifndef PACKAGE
#define PACKAGE "obj_detect"
#endif
#ifndef PACKAGE_NAME
#define PACKAGE_NAME "GStreamer Test Obj Detection Plugin"
#endif
#ifndef GST_PACKAGE_ORIGIN
#define GST_PACKAGE_ORIGIN "https://gstreamer.freedesktop.org/"
#endif

GST_PLUGIN_DEFINE(GST_VERSION_MAJOR,
                  GST_VERSION_MINOR,
                  obj_detect,
                  "Object detection test plugin",
                  plugin_init, VERSION, "LGPL", PACKAGE_NAME, GST_PACKAGE_ORIGIN)

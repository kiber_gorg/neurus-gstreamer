# About The Project
Custom plugin for Gstreamer. 
The plugin passes the incoming video stream through the localization model based on YOLOv3, applies the results to the video stream content. Enabling and disabling the results rendering is set by the plugin’s property.
# Required
- Gstreamer
- OpenCV
- CMake
- PkgConfig
- YOLOv3 .weigth and .cfg files
# Usage
gst-launch-1.0 filesrc location=/home/georgy/sample.mp4 ! qtdemux name=demux demux.video_0 ! queue ! avdec_h264 ! videoconvert ! obj_detect model-weights=/home/georgy/yolov3.weights model-config=/home/georgy/yolov3.cfg ! autovideosink
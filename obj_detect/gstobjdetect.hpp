#pragma once

#include <gst/gst.h>
#include <gst/video/video.h>
#include <gst/video/gstvideofilter.h>
#include <opencv2/dnn.hpp>

G_BEGIN_DECLS

#define GST_TYPE_OBJ_DETECT \
    (gst_obj_detect_get_type())
#define GST_OBJ_DETECT(obj) \
    (G_TYPE_CHECK_INSTANCE_CAST((obj), GST_TYPE_OBJ_DETECT, GstObjDetect))
#define GST_OBJ_DETECT_CLASS(klass) \
    (G_TYPE_CHECK_CLASS_CAST((klass), GST_TYPE_OBJ_DETECT, GstObjDetectClass))
#define GST_IS_OBJ_DETECT(obj) \
    (G_TYPE_CHECK_INSTANCE_TYPE((obj), GST_TYPE_OBJ_DETECT))
#define GST_IS_OBJ_DETECT_CLASS(klass) \
    (G_TYPE_CHECK_CLASS_TYPE((klass), GST_TYPE_OBJ_DETECT))
typedef struct _GstObjDetect GstObjDetect;
typedef struct _GstObjDetectClass GstObjDetectClass;

struct _GstObjDetect
{
    GstVideoFilter element;

    gboolean display;

    gchar *model_weights;
    gchar *model_config;
    gfloat nms_treshold;
    gfloat confidence_treshold;
    cv::dnn::Net network;
};

struct _GstObjDetectClass
{
    GstVideoFilterClass parent_class;
};

GType gst_obj_detect_get_type(void);

GST_ELEMENT_REGISTER_DECLARE(obj_detect)

G_END_DECLS
